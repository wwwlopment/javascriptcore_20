function send() {
  var name = document.getElementById('name').value;
  var age = document.getElementById('age').value;

  localStorage.setItem('name', name);
  localStorage.setItem('age', age);

  document.cookie = "name="+name+"; max-age=3600";
  document.cookie = "age="+age+"; max-age=3600";
}
